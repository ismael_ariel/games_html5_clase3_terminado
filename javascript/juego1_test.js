var Q = Quintus({
	development : true
});

Q.include("Sprites, Scenes, Input, 2D, Touch, Audio,Anim");
Q.setup("juego");
Q.enableSound();
Q.controls();

Q.gravityX = 0;
Q.gravityY = 0;

Q.load("mapa2.tmx, mosaicos.png", function() {

	Q.sheet("mosaicos", "mosaicos.png", {
		tileW : 20,
		tileH : 20
	});
	Q.stageScene("nivel1");
});

Q.animations("mosaicos_anim", {
	brillar : {
		frames : [27, 28],
		rate : 1 / 2
	}
});

Q.component("controlesPacman", {
	// default properties to add onto our entity
	defaults : {
		speed : 100,
		direction : 'up'
	},

	// called when the component is added to
	// an entity
	added : function() {
		var p = this.entity.p;

		p.direction = "left";
		p.speed = 100;

		// add in our default properties
		//Q._defaults(p, this.defaults);

		// every time our entity steps
		// call our step method
		this.entity.on("step", this, "step");
	},

	step : function(dt) {
		// grab the entity's properties
		// for easy reference
		var p = this.entity.p;

		// rotate the player
		// based on our velocity
		if (p.vx > 0) {
			p.angle = 0;
		} else if (p.vx < 0) {
			p.angle = 180;
		} else if (p.vy > 0) {
			p.angle = 90;
		} else if (p.vy < 0) {
			p.angle = 270;
		}

		// grab a direction from the input
		p.direction = Q.inputs['left'] ? 'left' : Q.inputs['right'] ? 'right' : Q.inputs['up'] ? 'up' : Q.inputs['down'] ? 'down' : p.direction;

		// based on our direction, try to add velocity
		// in that direction
		switch(p.direction) {
			case "left":
				p.vx = -p.speed;
				break;
			case "right":
				p.vx = p.speed;
				break;
			case "up":
				p.vy = -p.speed;
				break;
			case "down":
				p.vy = p.speed;
				break;
		}
	}
});

Q.Sprite.extend("Punto", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sensor : true
		});

		this.on("sensor");
	},
	sensor : function(colision) {
		
		//console.log("colision sensor:" + colision);
		this.destroy();
	}
});

Q.Sprite.extend("Poder", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim"
		});

		this.add("animation");
		this.play("brillar");
	}
});

//SOLUCIONES EJERCICIO1
Q.Sprite.extend("Alimento", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
		});
	}
});

Q.Sprite.extend("Pacman", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			tileW : 18,
			tileH : 18
		});

		this.add("2d, controlesPacman");
	}
});

Q.Sprite.extend("FantasmaRojo", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
		});
	}
});

Q.TileLayer.extend("Mapa", {
	init : function(p) {

		this._super({
			dataAsset : "mapa2.tmx",
			sheet : "mosaicos",
			tileW : 20,
			tileH : 20
		});

	},
	configurar : function() {
		//obtenemos la matriz de mosaicos
		var matrizMosaicos = this.p.tiles;
		//iteramos sobre todos los renglones
		for (var renglon = 0; renglon < matrizMosaicos.length; renglon++) {

			//obtenemos un renglon en cada iteracion
			var renglonMosaicos = matrizMosaicos[renglon];
			//por cada renglon, iteramos todas sus columnas
			for (var columna = 0; columna < renglonMosaicos.length; columna++) {
				//obtenemos el numero del mosaico
				var numeroMosaico = renglonMosaicos[columna];

				//calculamos la posicion de este mosaico
				var posX = columna * 20 + 10;
				var posY = renglon * 20 + 10;
				var nombreClase = null;
				//si el mosaico esta vacio, insertamos un punto

				switch(numeroMosaico) {
					case -1:
						nombreClase = "Punto";
						numeroMosaico = 29;
						break;
					case 27:
						nombreClase = "Poder";
						break;
					//SOLUCIONES EJERCICIOS
					case 92:
						//alimento
						nombreClase = "Alimento";
						break;
					case 30:
						//fantasma rojo
						nombreClase = "FantasmaRojo";
						break;
					case 70:
						//pacman
						nombreClase = "Pacman";
						break;
				}

				if (nombreClase != null) {

					//remplazamos el mosaico por un sprite
					this.stage.insert(new Q[nombreClase]({
						x : posX,
						y : posY,
						sheet : "mosaicos",
						frame : numeroMosaico
					}));

					//de la matriz de colisiones borramos el mosaico original
					renglonMosaicos[columna] = -1;
				}
			}
		}
	}
});

Q.scene("nivel1", function(stage) {

	var mapa = stage.collisionLayer(new Q.Mapa());
	mapa.configurar();
});
