var Q = Quintus({
	development : true
});

Q.include("Sprites, Scenes, Input, 2D, Touch, Audio, Anim");
Q.setup("juego");
Q.enableSound();
Q.controls();

Q.load("mapa.tmx, mosaicos.png", function() {

	Q.sheet("mosaicos", "mosaicos.png", {
		tileW : 20,
		tileH : 20
	});
	Q.stageScene("nivel1");
});

//solucion ejercicios
Q.animations("mosaicos_anim", {
	brillar : {
		frames : [27, 28],
		rate : 1 / 2
	}
});

Q.Sprite.extend("Punto", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos"
		});
	}
});

Q.Sprite.extend("Alimento", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos"
		});
	}
});

//SOLUCIONES EJERCICIO
Q.Sprite.extend("Poder", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim"
		});

		this.add("animation");
		this.play("brillar");
	}
});

Q.Sprite.extend("FantasmaRojo", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
		});
	}
});

Q.Sprite.extend("Pacman", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			tileW : 18,
			tileH : 18,
			speed : 100
		});

		this.add("2d, controlesPacman");

	}
});

Q.TileLayer.extend("Mapa", {
	init : function(p) {

		this._super({
			dataAsset : "mapa.tmx",
			sheet : "mosaicos",
			tileW : 20,
			tileH : 20
		});

	},
	configurar : function() {
		//obtenemos la matriz de mosaicos
		var matrizMosaicos = this.p.tiles;
		//iteramos sobre todos los renglones
		for (var renglon = 0; renglon < matrizMosaicos.length; renglon++) {

			//obtenemos un renglon en cada iteracion
			var renglonMosaicos = matrizMosaicos[renglon];
			//por cada renglon, iteramos todas sus columnas
			for (var columna = 0; columna < renglonMosaicos.length; columna++) {
				//obtenemos el numero del mosaico
				var numeroMosaico = renglonMosaicos[columna];

				//calculamos la posicion de este mosaico
				var posX = columna * 20 + 10;
				var posY = renglon * 20 + 10;
				var nombreClase = null;
				//si el mosaico esta vacio, insertamos un punto

				switch(numeroMosaico) {
					//alimentos
					case 90:
					//refresco
					//soluciones ejeercicio
					case 91:
					//pizza
					//soluciones ejeercicio
					case 92:
					//hamburguesa
					case 93:
						//papas
						nombreClase = "Alimento";
						break;
					case -1:						
						nombreClase = "Punto";
						numeroMosaico = 29;
						break;
					case 70:
						nombreClase = "Pacman";
						break;
					//SOLUCIONES EJERCICIOS
					case 27:
						nombreClase = "Poder";
						break;
					case 30:
						nombreClase = "FantasmaRojo";
						break;
				}

				if (nombreClase != null) {

					//remplazamos el mosaico por un sprite
					this.stage.insert(new Q[nombreClase]({
						x : posX,
						y : posY,
						sheet : "mosaicos",
						frame : numeroMosaico
					}));

					//de la matriz de colisiones borramos el mosaico original
					renglonMosaicos[columna] = -1;
				}
			}
		}
	}
});

Q.scene("nivel1", function(stage) {

	var mapa = stage.collisionLayer(new Q.Mapa());
	mapa.configurar();
});
