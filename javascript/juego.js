var Q = Quintus({
	development : true
});

Q.include("Sprites, Scenes, Input, 2D, Touch, Audio,Anim");
Q.setup("juego");
Q.enableSound();
Q.controls();

Q.load("mapa.tmx, mosaicos.png", function() {

	Q.sheet("mosaicos", "mosaicos.png", {
		tileW : 20,
		tileH : 20
	});
	Q.stageScene("nivel1");
});

Q.Sprite.extend("Punto", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			frame : 29
		});
	}
});

Q.scene("nivel1", function(stage) {

	stage.insert(new Q.Punto({
		x : 10,
		y : 10
	}));
});
