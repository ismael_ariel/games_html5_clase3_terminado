var Q = Quintus({
	development : true
});

Q.include("Sprites, Scenes, Input, 2D, Touch, Audio,Anim");
Q.setup("juego");
Q.enableSound();
Q.controls();
Q.touch();

Q.gravityX = 0;
Q.gravityY = 0;

var PACMAN = 1;
var MAPA = 2;
var ENEMIGO = 4;
var PUNTO = 8;
var ALIMENTO = 16;
var PODER = 32;

Q.load("mapa.tmx, mosaicos.png", function() {

	Q.sheet("mosaicos", "mosaicos.png", {
		tileW : 20,
		tileH : 20
	});
	Q.stageScene("nivel1", {
		sort : true
	});
});

Q.animations("mosaicos_anim", {
	brillar : {
		frames : [27, 28],
		rate : 1 / 2
	},
	pacman : {
		frames : [70, 71],
		rate : 1 / 3
	}
});

//SOLUCION EJERCICIO COMPONENTES
Q.component("aiEnemigo", {

	added : function() {
		var p = this.entity.p;

		p.velocidad = 100;
		p.direccion = "izquierda";
		p.porcentajeCambio = 0.02;

		this.entity.on("step", this, "controles");
		this.entity.on('hit', this, "cambiarEnAtasco");
	},

	controles : function() {

		var p = this.entity.p;

		// de manera aleatoria tratamos de cambiar direcciones
		if (Math.random() < p.porcentajeCambio) {
			this.cambiarEnMovimiento();
		}

		//incrementamos la velocidad
		//en funcion de la direccion
		switch(p.direccion) {
			case "izquierda":
				p.vx = -p.velocidad;
				break;
			case "derecha":
				p.vx = p.velocidad;
				break;
			case "arriba":
				p.vy = -p.velocidad;
				break;
			case "abajo":
				p.vy = p.velocidad;
				break;
		}
	},
	cambiarEnMovimiento : function() {
		var p = this.entity.p;
		//si se esta moviendo sobre el eje y
		if (p.vy != 0 && p.vx == 0) {
			//tratamos de movernos sobre el eje x
			p.direccion = Math.random() < 0.5 ? 'izquierda' : 'derecha';

			//si se esta moviendo sobre el eje x
		} else if (p.vx != 0 && p.vy == 0) {
			//tratamos de movernos sobre el eje y
			p.direccion = Math.random() < 0.5 ? 'arriba' : 'abajo';
		}
	},
	cambiarEnAtasco : function(colision) {
		var p = this.entity.p;
		//si el enemigo se atoro
		//su velocidad es cero en X y Y
		if (p.vx == 0 && p.vy == 0) {

			//la propiedad normalY de quintus es 1(arriba) o -1(abajo)
			if (colision.normalY) {

				//intenamos salir del atasco moviendonos a la derecha o izquierda
				p.direccion = Math.random() < 0.5 ? 'izquierda' : 'derecha';

				//la propiedad normalX de quintus es 1(izquierda) o -1(derecha)
			} else if (colision.normalX) {

				//intenamos salir del atasco moviendonos hacia arriba o abajo
				p.direccion = Math.random() < 0.5 ? 'arriba' : 'abajo';
			}
		}
	}
});

Q.component("controlesPacman", {

	//se llama cuando el componente es insertado
	added : function() {
		//obtenemos las propiedades del objeto
		var p = this.entity.p;

		//valores default
		p.direccion = "izquierda";
		//p.velocidad = 100;

		//forma de agregar valores
		Q._defaults(p, {
			velocidad : 100
		});

		this.entity.on("step", this, "controles");
	},

	controles : function() {
		//obtenemos las propiedades del objeto
		var p = this.entity.p;

		//determinamos la direccion
		p.direccion = Q.inputs['left'] ? 'izquierda' : Q.inputs['right'] ? 'derecha' : Q.inputs['up'] ? 'arriba' : Q.inputs['down'] ? 'abajo' : p.direccion;

		// based on our direccion, try to add velocity
		// in that direccion
		switch(p.direccion) {
			case "izquierda":
				p.vx = -p.velocidad;
				break;
			case "derecha":
				p.vx = p.velocidad;
				break;
			case "arriba":
				p.vy = -p.velocidad;
				break;
			case "abajo":
				p.vy = p.velocidad;
				break;
		}

		//rotar jugador
		if (p.vx > 0) {//derecha
			p.angle = 0;
		} else if (p.vx < 0) {//izquierda
			p.angle = 180;
		} else if (p.vy > 0) {//abajo
			console.log("abajo");
			p.angle = 90;
		} else if (p.vy < 0) {//arriba
			p.angle = 270;
		}

	}
});

Q.Sprite.extend("Punto", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sensor : true,
			type : PUNTO
		});

		this.on("sensor", function() {
			this.destroy();
		});
	}
});

Q.Sprite.extend("Alimento", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sensor : true,
			type : ALIMENTO
			//collisionMask : MASCARA_CAPA
		});

		this.on("sensor", function() {
			this.destroy();
		});
	}
});

//SOLUCIONES EJERCICIO
Q.Sprite.extend("Poder", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim",
			type : PODER,
			sensor : true
		});

		this.on("sensor", function() {
			this.destroy();
		});

		this.add("animation");
		this.play("brillar");
	}
});

Q.Sprite.extend("Fantasma", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			type : ENEMIGO,
			z : 10,
			collisionMask : PACMAN | MAPA
		});

		this.add("2d, aiEnemigo");
	}
});

Q.Sprite.extend("Pacman", {
	init : function(p) {

		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim",
			tileW : 18,
			tileH : 18,
			type : PACMAN,
			z : 20,
			velocidad : 100, //le pasamos directamente la velocidad
			collisionMask : ENEMIGO | ALIMENTO | PUNTO | PODER | MAPA
		});

		this.add("2d, controlesPacman, animation");
		this.play("pacman");

		this.on("hit", function(colision) {

			//si es un punto
			if (colision.tile === 29) {
				//borramos el mosaico de la capa de colisiones
				colision.obj.setTile(colision.tileX, colision.tileY, -1);
			}

		});
	}
});

Q.TileLayer.extend("Mapa", {
	init : function(p) {

		this._super({
			dataAsset : "mapa.tmx",
			sheet : "mosaicos",
			tileW : 20,
			tileH : 20,
			type : MAPA
			//collisionMask : MASCARA_CAPA
		});

	},
	configurar : function() {
		//obtenemos la matriz de mosaicos
		var matrizMosaicos = this.p.tiles;
		//iteramos sobre todos los renglones
		for (var renglon = 0; renglon < matrizMosaicos.length; renglon++) {

			//obtenemos un renglon en cada iteracion
			var renglonMosaicos = matrizMosaicos[renglon];
			//por cada renglon, iteramos todas sus columnas
			for (var columna = 0; columna < renglonMosaicos.length; columna++) {
				//obtenemos el numero del mosaico
				var numeroMosaico = renglonMosaicos[columna];

				//calculamos la posicion de este mosaico
				var posX = columna * 20 + 10;
				var posY = renglon * 20 + 10;
				var nombreClase = null;
				//si el mosaico esta vacio, insertamos un punto

				switch(numeroMosaico) {
					//alimentos
					case 90:
					//soluciones ejeercicio
					case 91:
					//soluciones ejeercicio
					case 92:
					//hamburguesa
					case 93:
						//papas
						nombreClase = "Alimento";
						break;
					case -1:
						nombreClase = "Punto";
						numeroMosaico = 29;
						break;
					case 70:
						nombreClase = "Pacman";
						break;
					//SOLUCIONES EJERCICIOS
					case 27:
						nombreClase = "Poder";
						break;
					case 30:
					//rojo
					case 40:
					//rosa
					case 50:
					//verde
					case 60:
						//naranja
						nombreClase = "Fantasma";
						break;
				}

				if (nombreClase != null) {

					//remplazamos el mosaico por un sprite
					this.stage.insert(new Q[nombreClase]({
						x : posX,
						y : posY,
						sheet : "mosaicos",
						frame : numeroMosaico
					}));

					//de la matriz de colisiones borramos el mosaico original
					renglonMosaicos[columna] = -1;
				}
			}
		}
	}
});

Q.scene("nivel1", function(stage) {

	var mapa = stage.collisionLayer(new Q.Mapa());
	mapa.configurar();
});
